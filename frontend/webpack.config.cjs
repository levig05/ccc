const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const styleHandler = MiniCssExtractPlugin.loader;

const config = {
  entry: "./js/main.js",
  output: {
    clean: true,
    path: path.resolve(__dirname, "dist"),
  },
  mode: "development",

  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "index.html", // HTML-Template ohne Referenzen zu JS oder CSS
    }),
    new MiniCssExtractPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [styleHandler, "css-loader"],
      },
    ],
  },

  devServer: {
    static: {
      directory: path.join(__dirname, "dist"), // Statische Dateien im dist-Ordner servieren
    },
    compress: true,
    open: true,
    port: 9000,
    hot: true, // Hot Module Replacement aktivieren
    historyApiFallback: true,
  },
};

module.exports = () => {
  return config;
};
