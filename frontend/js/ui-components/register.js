import { register as registerUser } from "../services/authService.js";
import { CLASS_DO_NOT_DISPLAY } from "../utils/classesForStyling.js";
import { displayError } from "../utils/elementsUtil.js";
import { init as initLogin } from "./login.js";

const ID_FORM = "form-register";
const ID_USERNAME = "username";
const ID_PASSWORD = "password";
const ID_SUBMIT = "submit";
const ID_TO_LOGIN = "to-login";
const ID_FAILED_REGISTER = "register-failed";

function init() {
  document.body.innerHTML = `<div class="container-form">
        <h1>Registrieren und TODOs erstellen</h1>
        <form class="form" id="${ID_FORM}">
          <label class="small-font" for="${ID_USERNAME}">Username</label>
          <br />
          <input 
            type="text"
            class="fit-container"
            name="username"
            id="${ID_USERNAME}" 
          />
          <br />
          <label class="small-font" for="${ID_PASSWORD}">Password</label>
          <br />
          <input
            type="password"
            class="fit-container"
            name="password"
            id="${ID_PASSWORD}"
            autocomplete="new-password"
          />
          <br />
          <input 
            type="submit"
            class="btn fit-container"
            id="${ID_SUBMIT}"
            value="Registrieren"
          />
        <p id="${ID_FAILED_REGISTER}" class="error ${CLASS_DO_NOT_DISPLAY}"></p>
        </form>
        <button 
          id="${ID_TO_LOGIN}"
          class="link small-font center fit-container"
        >
            Schon ein Account? Zum Login.
        </button>
    </div>`;
  const elToLogin = document.getElementById(ID_TO_LOGIN);
  elToLogin.addEventListener("click", initLogin);
  const elRegister = document.getElementById(ID_SUBMIT);
  elRegister.addEventListener("click", (event) => {
    event.preventDefault();
    register(
      document.getElementById(ID_USERNAME).value,
      document.getElementById(ID_PASSWORD).value
    );
  });
}

async function register(username, password) {
  const elError = document.getElementById(ID_FAILED_REGISTER);
  if (username && password) {
    await registerUser({ username: username, pwd: password })
      .then(() => {
        initLogin();
      })
      .catch(() => displayError("Registrierung fehlgeschlagen"), elError);
  } else {
    displayError("Angaben fehlen", elError);
  }
}

export { init };
