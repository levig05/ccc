import { login as loginUser } from "../services/authService.js";
import { CLASS_DO_NOT_DISPLAY } from "../utils/classesForStyling.js";
import { displayError } from "../utils/elementsUtil.js";
import { init as initRegister } from "./register.js";
import { initMyEvents as initEvents } from "./events.js";

const ID_FORM = "form-login";
const ID_USERNAME = "e-mail";
const ID_PWD = "password";
const ID_SUBMIT = "submit";
const ID_TO_REGISTER = "to-register";
const ID_FAILED_LOGIN = "login-failed";

function init() {
  document.body.innerHTML = `<div class="container-form">
        <h1>Einloggen</h1>
        <form class="form" id="${ID_FORM}" autocomplete="new-password">
          <label class="small-font" for="${ID_USERNAME}">Username</label>
          <br />
          <input
            type="text"
            class="fit-container"
            name="username"
            id="${ID_USERNAME}"
          />
          <br />
          <label class="small-font" for="${ID_PWD}">Password</label>
          <br />
          <input 
            type="password"
            class="fit-container"
            name="pwd"
            id="${ID_PWD}"
            autocomplete="new-password"
          />
          <br />
          <input
            type="submit"
            class="btn fit-container"
            id="${ID_SUBMIT}"
            value="Login"
          />
          <p id="${ID_FAILED_LOGIN}" class="error ${CLASS_DO_NOT_DISPLAY}"></p>
        </form>
        <button 
          id="${ID_TO_REGISTER}"
          class="link small-font center fit-container"
        >
            Neu hier? Login erstellen.
        </button>
    </div>`;
  const elToRegister = document.getElementById(ID_TO_REGISTER);
  elToRegister.addEventListener("click", initRegister);
  const elLogin = document.getElementById(ID_SUBMIT);
  elLogin.addEventListener("click", (event) => {
    event.preventDefault();
    login(
      document.getElementById(ID_USERNAME).value,
      document.getElementById(ID_PWD).value
    );
  });
}

async function login(username, pwd) {
  const elError = document.getElementById(ID_FAILED_LOGIN);
  if (username && pwd) {
    await loginUser({ username: username, pwd: pwd })
      .then((loggedInUser) => {
        initEvents(loggedInUser);
      })
      .catch(() => displayError("Login fehlgeschlagen", elError));
  } else {
    displayError("Angaben fehlen", elError);
  }
}

export { init };
