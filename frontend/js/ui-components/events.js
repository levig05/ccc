import {
  getMyEvents,
  getFriendsEvents,
  getPublicEvents,
  create as saveEvent,
  update as updateEvent,
  remove as deleteEvent,
} from "../services/eventsService.js";
import { logout as logoutUser } from "../services/authService.js";
import { init as initLogin } from "./login.js";
import {
  getUserIdByUsername,
  getUsernameById,
} from "../services/userService.js";

let data;
const body = document.getElementsByTagName("body")[0];
let sortVal;
let searchTextVal;

async function initMyEvents(loginUser, tabValue) {
  const elementToRemove = document.getElementsByClassName("container-form")[0];
  if (elementToRemove) {
    elementToRemove.remove();
  }
  const alreadyExistingEventSection = document.getElementById("eventSection");
  const alreadyExistingHeader = document.getElementsByTagName("header")[0];
  if (alreadyExistingEventSection) {
    alreadyExistingEventSection.remove();
  }
  if (alreadyExistingHeader) {
    alreadyExistingHeader.remove();
  }
  await doHeader(loginUser);
  if (tabValue === "my-events") {
    data = await getMyEvents(loginUser._id);
  } else if (tabValue === "friends-events") {
    data = await getFriendsEvents(loginUser._id);
  } else {
    data = await getPublicEvents(loginUser._id);
  }
  //console.log(data);
  formatData(tabValue, loginUser);
}

async function doHeader(loginUser) {
  const header = document.createElement("header");

  const titleText = "Crazy Carnival Calendar";
  const titleTextSub = "CCC - your personal eventcalendar";
  const titleDiv = document.createElement("div");
  const bigTitleEl = document.createElement("h1");
  bigTitleEl.textContent = titleText;
  const subTitleEl = document.createElement("h3");
  subTitleEl.textContent = titleTextSub;
  titleDiv.appendChild(bigTitleEl);
  titleDiv.appendChild(subTitleEl);
  header.appendChild(titleDiv);

  const logOutBtn = document.createElement("button");
  logOutBtn.textContent = "Logout";
  logOutBtn.id = "logOutBtn";
  logOutBtn.addEventListener("click", async () => {
    await logoutUser().then(() => {
      initLogin();
    });
  });

  const eventBtnSection = document.createElement("div");
  const myEventsBtn = document.createElement("button");
  myEventsBtn.textContent = "My Events";
  myEventsBtn.addEventListener("click", () => {
    initMyEvents(loginUser, "my-events");
  });
  eventBtnSection.appendChild(myEventsBtn);

  const friendsEventsBtn = document.createElement("button");
  friendsEventsBtn.textContent = "Friends' Events";
  friendsEventsBtn.addEventListener("click", () => {
    initMyEvents(loginUser, "friends-events");
  });
  eventBtnSection.appendChild(friendsEventsBtn);

  const publicEventsBtn = document.createElement("button");
  publicEventsBtn.textContent = "Public Events";
  publicEventsBtn.addEventListener("click", () => {
    initMyEvents(loginUser, "public-events");
  });
  eventBtnSection.appendChild(publicEventsBtn);

  titleDiv.appendChild(eventBtnSection);
  header.appendChild(logOutBtn);

  body.appendChild(header);
}

async function filterDataWithSearchInput() {
  const searchText = document.getElementById("search-text").value;
  searchTextVal = searchText;
  data.filter(async (dataLine) => {
    //console.log(dataLine.user);
    let eventUsername = await getUsernameById(dataLine.user);
    eventUsername = eventUsername.username;
    console.log(searchText, searchTextVal);
    //console.log(eventUsername, searchText, eventUsername.includes(searchText));
    if (
      dataLine.description.what.includes(searchText) ||
      dataLine.description.when.includes(searchText) ||
      dataLine.description.where.includes(searchText) ||
      eventUsername.includes(searchText)
    ) {
      return dataLine;
    }
  });
}

function sortDataBySortElValue(sortValue) {
  //console.log(data);
  //console.log(sortValue);
  if (sortValue === "sortByDate") {
    data.sort(
      (a, b) => new Date(a.description.when) - new Date(b.description.when)
    );
  } else if (sortValue === "sortByCost-lowToHigh") {
    data.sort((a, b) => a.description.cost - b.description.cost);
  } else if (sortValue === "sortByCost-highToLow") {
    data.sort((a, b) => b.description.cost - a.description.cost);
  }
  sortVal = sortValue;
  //console.log(data);
}

function formatData(tabValue, loginUser) {
  if (document.getElementById("eventSection")) {
    const divToDelete = document.getElementById("eventSection");
    divToDelete.remove();
  }
  const eventSection = document.createElement("div");
  eventSection.id = "eventSection";
  const searchInput = document.createElement("input");
  searchInput.type = "text";
  searchInput.id = "search-text";
  searchInput.placeholder = "Search for Event using Keywords...";
  console.log("hello world", searchTextVal);
  if (searchTextVal) {
    console.log("hello world");
    searchInput.value = searchTextVal;
  }
  const searchBtn = document.createElement("button");
  searchBtn.textContent = "search";
  searchBtn.addEventListener("click", async () => {
    await filterDataWithSearchInput();
    formatData(tabValue, loginUser);
  });
  eventSection.appendChild(searchInput);
  eventSection.appendChild(searchBtn);

  const sortEl = document.createElement("select");
  sortEl.id = "sortEvents";

  const sortOp1 = document.createElement("option");
  sortOp1.value = "none";
  sortOp1.textContent = "no sort";

  const sortOp2 = document.createElement("option");
  sortOp2.value = "sortByDate";
  sortOp2.textContent = "date";

  const sortOp3 = document.createElement("option");
  sortOp3.value = "sortByCost-lowToHigh";
  sortOp3.textContent = "cost: low - high";

  const sortOp4 = document.createElement("option");
  sortOp4.value = "sortByCost-highToLow";
  sortOp4.textContent = "cost: high - low";

  sortEl.appendChild(sortOp1);
  sortEl.appendChild(sortOp2);
  sortEl.appendChild(sortOp3);
  sortEl.appendChild(sortOp4);
  if (sortVal) {
    let arr = [sortOp1, sortOp2, sortOp3, sortOp4];
    arr = arr.filter((el) => el.value === sortVal);
    let arrEl = arr[0];
    arrEl.selected = true;
  } else {
    sortOp1.selected = true;
  }
  sortEl.addEventListener("change", async (e) => {
    //console.log(e.target.value);
    await sortDataBySortElValue(e.target.value);
    formatData();
  });
  eventSection.appendChild(sortEl);

  if (tabValue === "my-events") {
    const newDivForAddBtn = document.createElement("div");
    const addBtn = document.createElement("button");
    addBtn.id = "addEventBtn";
    addBtn.textContent = "+";
    addBtn.addEventListener("click", () => {
      createAddForm(tabValue, loginUser);
    });
    newDivForAddBtn.appendChild(addBtn);
    eventSection.appendChild(newDivForAddBtn);

    const eventSecText = document.createElement("h4");
    eventSecText.textContent = "my events:";
    eventSection.appendChild(eventSecText);
  } else if (tabValue === "friends-events") {
    const eventSecText = document.createElement("h4");
    eventSecText.textContent = "my friends events:";
    eventSection.appendChild(eventSecText);
  } else {
    const eventSecText = document.createElement("h4");
    eventSecText.textContent = "public events:";
    eventSection.appendChild(eventSecText);
  }
  if (data.length === 0) {
    const eventBox = document.createElement("div");
    eventBox.textContent = "no events found";
    eventSection.appendChild(eventBox);
  } else {
    data.forEach((event) => {
      const eventBox = document.createElement("div");
      eventBox.classList.add("event-box");

      // Erstellen der Beschreibungselemente
      const eventDescription = document.createElement("div");
      eventDescription.classList.add("event-description");
      eventDescription.addEventListener("click", () =>
        showEventDetails(event._id)
      );
      const eventTitle = document.createElement("h3");
      eventTitle.textContent = event.description.what;
      eventDescription.appendChild(eventTitle);

      // Erstellen der Details zum Ereignis
      const eventDetails = document.createElement("div");
      eventDetails.classList.add("event-details");
      eventDetails.id = `event-details-${event._id}`;
      eventDetails.style.display = "none";
      const eventDescriptionParagraph = document.createElement("p");
      eventDescriptionParagraph.innerHTML = `<strong>Beschreibung:</strong> ${event.description.what}`;
      const eventLocationParagraph = document.createElement("p");
      eventLocationParagraph.innerHTML = `<strong>Ort:</strong> ${event.description.where}`;
      const eventDateParagraph = document.createElement("p");
      eventDateParagraph.innerHTML = `<strong>Datum:</strong> ${new Date(
        event.description.when
      ).toLocaleDateString()}`;
      const eventCostParagraph = document.createElement("p");
      eventCostParagraph.innerHTML = `<strong>Kosten:</strong> ${event.description.cost}`;

      const closeButton = document.createElement("button");
      closeButton.textContent = "close";
      closeButton.addEventListener("click", () => hideEventDetails(event._id));

      eventDetails.appendChild(eventDescriptionParagraph);
      eventDetails.appendChild(eventLocationParagraph);
      eventDetails.appendChild(eventDateParagraph);
      eventDetails.appendChild(eventCostParagraph);
      eventDetails.appendChild(closeButton);

      if (tabValue === "my-events") {
        const deleteBtn = document.createElement("button");
        deleteBtn.textContent = "delete";
        deleteBtn.addEventListener("click", async () => {
          //console.log("click", loginUser._id, event._id);
          let validatePrompt = prompt(
            "Bitte Gebe '" +
              "JA" +
              "' ein um zu bestätigen, dass du dieses Event löschen willst."
          );
          validatePrompt = validatePrompt ? validatePrompt.toUpperCase() : "";

          if (validatePrompt === "JA") {
            console.log("Bestätigt");
            // delete einbauen
            await deleteEvent(event._id, loginUser._id);
            initMyEvents(loginUser, tabValue);
          } else {
            console.log("Nicht bestätigt oder ungültige Eingabe.");
          }
        });
        eventDetails.appendChild(deleteBtn);
        const editBtn = document.createElement("button");
        editBtn.textContent = "edit";
        //editBtn.id = event._id;
        editBtn.addEventListener("click", () => {
          prepareEdit(event, loginUser, tabValue);
        });
        eventDetails.appendChild(editBtn);
      }

      // Anhängen der erstellten Elemente
      eventBox.appendChild(eventDescription);
      eventBox.appendChild(eventDetails);

      eventSection.appendChild(eventBox);
    });
  }

  body.appendChild(eventSection);
}

function showEventDetails(eventId) {
  document.getElementById(`event-details-${eventId}`).style.display = "block";
}

function hideEventDetails(eventId) {
  document.getElementById(`event-details-${eventId}`).style.display = "none";
}

async function prepareEdit(event, loginUser, tabValue) {
  const eventDetails = document.getElementById(`event-details-${event._id}`);
  eventDetails.innerHTML = ""; // Clear existing content

  const editForm = document.createElement("div");
  editForm.classList.add("edit-form");

  const whatLabel = document.createElement("label");
  whatLabel.textContent = "Beschreibung:";
  const whatInput = document.createElement("input");
  whatInput.type = "text";
  whatInput.value = event.description.what;

  const whereLabel = document.createElement("label");
  whereLabel.textContent = "Ort:";
  const whereInput = document.createElement("input");
  whereInput.type = "text";
  whereInput.value = event.description.where;

  let newIsPublic = event.isPublic;

  const visibilityLabel = document.createElement("label");
  visibilityLabel.textContent = "Sichtbarkeit:";

  // Erstellen des Dropdown-Menüs
  const visibilityDropdown = document.createElement("select");

  // Option für öffentliche Events hinzufügen
  const publicOption = document.createElement("option");
  publicOption.value = "true";
  publicOption.textContent = "public";
  visibilityDropdown.appendChild(publicOption);

  // Option für private Events hinzufügen
  const privateOption = document.createElement("option");
  privateOption.value = "false";
  privateOption.textContent = "private";
  visibilityDropdown.appendChild(privateOption);

  // Standardwert des Dropdown-Menüs basierend auf dem Event setzen
  visibilityDropdown.value = event.isPublic ? "true" : "false";

  // Eventlistener hinzufügen, um die Auswahl zu verfolgen
  visibilityDropdown.addEventListener("change", (e) => {
    const selectedValue = e.target.value;
    //console.log(selectedValue);
    if (selectedValue === "false") {
      newIsPublic = false;
    } else {
      newIsPublic = true;
    }
    // Hier kannst du die Logik einfügen, um das Event entsprechend der Auswahl zu aktualisieren
  });

  const whenLabel = document.createElement("label");
  whenLabel.textContent = "Datum:";
  const whenInput = document.createElement("input");
  whenInput.type = "date";
  const eventDate = new Date(event.description.when);
  const formattedDate = `${eventDate.getFullYear()}-${(eventDate.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${eventDate.getDate().toString().padStart(2, "0")}`;
  whenInput.value = formattedDate;

  const costLabel = document.createElement("label");
  costLabel.textContent = "Kosten:";
  const costInput = document.createElement("input");
  costInput.type = "number";
  costInput.value = event.description.cost;
  let newIsAllowedToSee = event.isAllowedToSee;
  const addFriendBtn = document.createElement("button");
  addFriendBtn.textContent = "Freund hinzufügen";
  addFriendBtn.addEventListener("click", async () => {
    const friendName = prompt("Geben Sie den Username ihres Freundes ein:");
    if (friendName) {
      let friendId = await getUserIdByUsername(friendName);
      //console.log(friendId);
      friendId = "" + friendId._id;
      //console.log(friendId);
      if (newIsAllowedToSee.indexOf(friendId) === -1) {
        newIsAllowedToSee.push(friendId);
        console.log(
          "Freund hinzugefügt:",
          newIsAllowedToSee[newIsAllowedToSee.length - 1]
        );
      } else {
        console.log(
          "Freund kann dieses Event bereits sehen:",
          newIsAllowedToSee[newIsAllowedToSee.indexOf(friendId)]
        );
      }
    }
  });

  const confirmBtn = document.createElement("button");
  confirmBtn.textContent = "confirm";
  confirmBtn.addEventListener("click", async () => {
    const newWhat = whatInput.value.trim();
    const newWhere = whereInput.value.trim();
    const newDate = whenInput.value.trim();
    const newCost = parseInt(costInput.value);

    // Überprüfen, ob alle erforderlichen Felder ausgefüllt sind
    if (newWhat === "" || newWhere === "" || newDate === "" || isNaN(newCost)) {
      alert("Bitte füllen Sie alle Felder korrekt aus.");
      return;
    }
    //console.log(newIsPublic);
    // Neues Event-Objekt erstellen
    const newEvent = {
      _id: event._id, // Event-ID beibehalten
      description: {
        what: newWhat,
        where: newWhere,
        when: new Date(newDate),
        cost: newCost,
      },
      isAllowedToSee: newIsAllowedToSee,
      isPublic: newIsPublic,
      user: loginUser._id,
    };
    //console.log(newEvent.description.when);
    await updateEvent(newEvent._id, newEvent, loginUser._id);
    initMyEvents(loginUser, tabValue);
  });

  const cancelBtn = document.createElement("button");
  cancelBtn.textContent = "cancel";
  cancelBtn.addEventListener("click", () => {
    // Hier die Logik für das Abbrechen der Bearbeitung einfügen
    // Möglicherweise einfach die Details wieder anzeigen lassen
    //showEventDetails(event._id);
    initMyEvents(loginUser, tabValue);
  });

  // Alle erstellten Elemente dem Formular hinzufügen
  editForm.appendChild(whatLabel);
  editForm.appendChild(whatInput);
  editForm.appendChild(whereLabel);
  editForm.appendChild(whereInput);
  editForm.appendChild(whenLabel);
  editForm.appendChild(whenInput);
  editForm.appendChild(costLabel);
  editForm.appendChild(costInput);
  editForm.appendChild(visibilityLabel);
  editForm.appendChild(visibilityDropdown);
  editForm.appendChild(addFriendBtn);
  editForm.appendChild(confirmBtn);
  editForm.appendChild(cancelBtn);

  // Das Formular dem Event-Detailbereich hinzufügen
  eventDetails.appendChild(editForm);
}

async function createAddForm(tabValue, loginUser) {
  const eventSection = document.getElementById("eventSection");
  eventSection.innerHTML = "";

  const addForm = document.createElement("div");
  addForm.classList.add("add-form");

  const whatLabel = document.createElement("label");
  whatLabel.textContent = "Beschreibung:";
  const whatInput = document.createElement("input");
  whatInput.type = "text";

  const whereLabel = document.createElement("label");
  whereLabel.textContent = "Ort:";
  const whereInput = document.createElement("input");
  whereInput.type = "text";

  let newIsPublic = false; // Standardwert für neue Events

  const visibilityLabel = document.createElement("label");
  visibilityLabel.textContent = "Sichtbarkeit:";

  // Erstellen des Dropdown-Menüs für die Sichtbarkeit
  const visibilityDropdown = document.createElement("select");

  // Option für private Events hinzufügen
  const privateOption = document.createElement("option");
  privateOption.value = "false";
  privateOption.textContent = "private";
  visibilityDropdown.appendChild(privateOption);

  // Option für öffentliche Events hinzufügen
  const publicOption = document.createElement("option");
  publicOption.value = "true";
  publicOption.textContent = "public";
  visibilityDropdown.appendChild(publicOption);

  // Eventlistener hinzufügen, um die Auswahl zu verfolgen
  visibilityDropdown.addEventListener("change", (e) => {
    const selectedValue = e.target.value;
    if (selectedValue === "false") {
      newIsPublic = false;
    } else {
      newIsPublic = true;
    }
  });

  const whenLabel = document.createElement("label");
  whenLabel.textContent = "Datum:";
  const whenInput = document.createElement("input");
  whenInput.type = "date";

  const costLabel = document.createElement("label");
  costLabel.textContent = "Kosten:";
  const costInput = document.createElement("input");
  costInput.type = "number";

  let newIsAllowedToSee = []; // Liste der Freunde, die das Event sehen dürfen

  const addFriendBtn = document.createElement("button");
  addFriendBtn.textContent = "Freund hinzufügen";
  addFriendBtn.addEventListener("click", async () => {
    const friendName = prompt("Geben Sie den Username ihres Freundes ein:");
    if (friendName) {
      let friendId = await getUserIdByUsername(friendName);
      friendId = "" + friendId._id;
      if (newIsAllowedToSee.indexOf(friendId) === -1) {
        newIsAllowedToSee.push(friendId);
        console.log(
          "Freund hinzugefügt:",
          newIsAllowedToSee[newIsAllowedToSee.length - 1]
        );
      } else {
        console.log(
          "Freund kann dieses Event bereits sehen:",
          newIsAllowedToSee[newIsAllowedToSee.indexOf(friendId)]
        );
      }
    }
  });

  const saveBtn = document.createElement("button");
  saveBtn.textContent = "save";
  saveBtn.addEventListener("click", async () => {
    const newWhat = whatInput.value.trim();
    const newWhere = whereInput.value.trim();
    const newDate = whenInput.value.trim();
    const newCost = parseInt(costInput.value);

    // Überprüfen, ob alle erforderlichen Felder ausgefüllt sind
    if (newWhat === "" || newWhere === "" || newDate === "" || isNaN(newCost)) {
      alert("Bitte füllen Sie alle Felder korrekt aus.");
      return;
    }

    // Neues Event-Objekt erstellen
    const newEvent = {
      description: {
        what: newWhat,
        where: newWhere,
        when: new Date(newDate),
        cost: newCost,
      },
      isAllowedToSee: newIsAllowedToSee, // Keine Freunde hinzugefügt für neue Events
      isPublic: newIsPublic,
      user: loginUser._id,
    };

    // Neues Event speichern
    await saveEvent(newEvent);
    //console.log(newEvent);
    // Seite aktualisieren
    initMyEvents(loginUser, tabValue);
  });

  const cancelBtn = document.createElement("button");
  cancelBtn.textContent = "cancel";
  cancelBtn.addEventListener("click", () => {
    // Hier die Logik für das Abbrechen der Hinzufügung einfügen
    // Möglicherweise einfach den Add-Formularbereich leeren
    eventSection.removeChild(addForm);
  });

  // Alle erstellten Elemente dem Formular hinzufügen
  addForm.appendChild(whatLabel);
  addForm.appendChild(whatInput);
  addForm.appendChild(whereLabel);
  addForm.appendChild(whereInput);
  addForm.appendChild(whenLabel);
  addForm.appendChild(whenInput);
  addForm.appendChild(costLabel);
  addForm.appendChild(costInput);
  addForm.appendChild(visibilityLabel);
  addForm.appendChild(visibilityDropdown);
  addForm.appendChild(addFriendBtn);
  addForm.appendChild(saveBtn);
  addForm.appendChild(cancelBtn);

  // Das Formular dem Event-Bereich hinzufügen
  eventSection.appendChild(addForm);
}

export { initMyEvents, showEventDetails, hideEventDetails };
