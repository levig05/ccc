import axios from "axios";
const BASE_URL = "/api/data";

const getMyEvents = (userId) => {
  //console.log("hello world");
  const request = axios.get(BASE_URL + "/events/mine/" + userId);
  return request.then((response) => response.data);
};

const getFriendsEvents = (userId) => {
  const request = axios.get(BASE_URL + "/events/friends/" + userId);
  return request.then((response) => response.data);
};

const getPublicEvents = (userId) => {
  const request = axios.get(BASE_URL + "/events/public/" + userId);
  return request.then((response) => response.data);
};

const create = (newEvent) => {
  const request = axios.post(`${BASE_URL}/new/event`, newEvent);
  return request.then((response) => response.data);
};

const update = (id, EventToUpdate, userId) => {
  const request = axios.put(`${BASE_URL}/${id}/${userId}`, EventToUpdate);
  return request.then((response) => response.data);
};

const remove = (id, userId) => {
  const request = axios.delete(`${BASE_URL}/${id}/${userId}`);
  return request.then((response) => response.data);
};

export {
  getMyEvents,
  getFriendsEvents,
  getPublicEvents,
  create,
  update,
  remove,
};
