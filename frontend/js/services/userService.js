import axios from "axios";
const BASE_URL = "/api/user";

async function getUserIdByUsername(username) {
  const request = axios.get(BASE_URL + "/username/" + username);
  return request.then((response) => response.data);
}

async function getUsernameById(id) {
  //console.log(id);
  const request = axios.get(BASE_URL + "/id/" + id);
  return request.then((response) => response.data);
}

export { getUserIdByUsername, getUsernameById };
