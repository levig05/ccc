import { getLoggedInUser } from "./services/authService.js";
import { init as initLogin } from "./ui-components/login.js";
import { initMyEvents } from "./ui-components/events.js";
import "../css/style.css";

getLoggedInUser()
  .then((user) => {
    if (!user) {
      return initLogin();
    }
    initMyEvents(user, "my-events");
  })
  .catch(() => initLogin());
