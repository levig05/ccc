function isUserAllowedToChangeEvent(userId, creatorId) {
  if (userId === creatorId) {
    return true;
  } else {
    return false;
  }
}

export { isUserAllowedToChangeEvent };
