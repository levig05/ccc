import { model as User } from "../user/user.model.js";
import { model as Event } from "../data/data.model.js";

function isAuthenticated(request, response, next) {
  if (!request.isAuthenticated()) {
    return response.status(401).json({ message: "Not authenticated" });
  }
  next();
}
/*
async function isAllowedToChangeTeam(request, response, next) {
  const userId = request.user._id;
  const teamId = request.params.id;
  const foundTeam = await Team.findById(teamId);
  if (!foundTeam) {
    return response
      .status(400)
      .json({ message: `Team with id ${teamId} does not exist.` });
  }
  if (!isUserTeamAdmin(userId, foundTeam)) {
    return response.status(401).json({ message: "Not allowed to change team" });
  }
  // Aus Performance Gründen: In DB geladenes Document Request anhängen,
  // damit dies im Controller nicht nochamls gemacht werden muss
  request.team = foundTeam;
  next();
}
*/
async function isAllowedToChangeEvent(request, response, next) {
  const userId = request.params.userId;
  const eventId = request.params.id;
  const [foundEvent] = await Promise.all([Event.findById(eventId).exec()]);
  if (!foundEvent) {
    return response
      .status(400)
      .json({ message: `Todo with id ${eventId} does not exist.` });
  }
  if (!isUserAllowedToChangeEvent(userId, foundEvent.user)) {
    return response.status(401).json({ message: "Not allowed to change todo" });
  }
  // Aus Performance Gründen: In DB geladene Documents Request anhängen,
  // damit dies im Controller nicht nochamls gemacht werden muss
  //console.log(request.event);
  request.event = foundEvent;
  //console.log(request.event);
  next();
}

function isUserAllowedToChangeEvent(userId, eventUser) {
  //console.log(userId, eventUser, typeof userId, typeof eventUser);
  if (userId === eventUser) {
    return true;
  }
  return false;
}

/*
function isUserTeamAdmin(userId, team) {
  const foundAdmin = team.admins.find((adminId) => adminId.equals(userId));
  return !!foundAdmin;
}
*/

export { isAuthenticated, isAllowedToChangeEvent };
