import { model as User } from "./user/user.model.js";
import { model as Event } from "./data/data.model.js";

function findUserByIdAndCheckThatItExists(userId) {
  return findDocAndCheckThatItExists(
    User.findById(userId),
    `User with id ${userId} does not exist.`
  );
}

function findUserByNameAndCheckThatItExists(username) {
  return findDocAndCheckThatItExists(
    User.findOne({ username: username }),
    `User with username ${username} does not exist.`
  );
}

function findTeamByIdAndCheckThatItExists(eventId) {
  return findDocAndCheckThatItExists(
    Team.findById(eventId),
    `Team with id ${eventId} not found.`
  );
}

function findDocAndCheckThatItExists(query, errorMsg) {
  return query.exec().then((foundDocument) => {
    if (!foundDocument) {
      return Promise.reject({
        message: errorMsg,
        status: 400,
      });
    }
    return foundDocument;
  });
}

function handleError(error, response) {
  const status = error.status;
  if (status) {
    response.status(status);
  } else {
    response.status(500);
  }
  response.json({ message: error.message });
}

function removeIdFromArrayOfIds(ids, idToRemove) {
  const index = ids.findIndex((id) => id.equals(idToRemove));
  if (index >= 0) {
    ids.splice(index, 1);
  }
}

export {
  findUserByIdAndCheckThatItExists,
  findUserByNameAndCheckThatItExists,
  findTeamByIdAndCheckThatItExists,
  handleError,
  removeIdFromArrayOfIds,
};
