import { model as Event } from "./data.model.js";
import mongoose from "mongoose";
import {
  findUserByNameAndCheckThatItExists,
  handleError,
  removeIdFromArrayOfIds,
} from "../controllerUtils.js";

function getAllMyEvents(req, res) {
  const userId = req.params.userId;
  //console.log(userId);
  Event.find({ user: userId })
    .then((events) => {
      //console.log(events);
      res.json(events);
    })
    .catch((error) => {
      handleError(res, error);
    });
}

function getAllFriendsEvents(req, res) {
  //console.log("hello world");
  const userId = req.params.userId;
  Event.find({ isAllowedToSee: userId })
    .then((events) => {
      //console.log(events);
      res.json(events.filter((event) => event.user !== userId));
    })
    .catch((error) => {
      handleError(res, error);
    });
}

function getAllPublicEvents(req, res) {
  const userId = req.params.userId;
  //console.log(userId);
  Event.find({ isPublic: true })
    .then((events) => {
      res.json(events.filter((event) => event.user !== userId));
    })
    .catch((error) => {
      handleError(res, error);
    });
}

async function updateEvent(req, res) {
  //console.log(req.params);
  //console.log(req.body);
  const eventId = req.params.id;
  const updatedEventData = req.body;
  try {
    const updatedEvent = await Event.findByIdAndUpdate(
      eventId,
      updatedEventData,
      { new: true }
    );

    if (!updatedEvent) {
      return res.status(404).json({ message: "Event not found." });
    }

    return res.status(200).json(updatedEvent);
  } catch (error) {
    console.error("Error updating event:", error);
    return res.status(500).json({ message: "Internal server error." });
  }
}

async function removeEvent(req, res) {
  const eventId = req.params.id;
  console.log(eventId);
  //return res.json("");
  try {
    // Finde das Event anhand der eventId und entferne es
    const deletedEvent = await Event.findByIdAndRemove(eventId);

    if (!deletedEvent) {
      return res.status(404).json({ message: "Event not found." });
    }

    return res.status(200).json({ message: "Event deleted successfully." });
  } catch (error) {
    console.error("Error removing event:", error);
    return res.status(500).json({ message: "Internal server error." });
  }
}

async function createNewEvent(req, res) {
  const eventData = req.body; // Die Daten des neuen Events aus dem Request

  try {
    // Erstelle ein neues Event mit den erhaltenen Daten
    const newEvent = new Event(eventData);

    // Speichere das neue Event in der Datenbank
    const savedEvent = await newEvent.save();

    // Gib das gespeicherte Event als Antwort zurück
    return res.status(201).json(savedEvent);
  } catch (error) {
    console.error("Error creating new event:", error);
    return res.status(500).json({ message: "Internal server error." });
  }
}

export {
  getAllMyEvents,
  getAllFriendsEvents,
  getAllPublicEvents,
  updateEvent,
  removeEvent,
  createNewEvent,
};
