import { Router } from "express";
import {
  getAllMyEvents,
  getAllFriendsEvents,
  getAllPublicEvents,
  updateEvent,
  removeEvent,
  createNewEvent,
} from "./data.controller.js";
import { isAllowedToChangeEvent } from "../auth/authorization.js";

const router = Router();

router.get("/events/mine/:userId", getAllMyEvents);
router.get("/events/friends/:userId", getAllFriendsEvents);
router.get("/events/public/:userId", getAllPublicEvents);
router.post("/new/event", createNewEvent);
router.put("/:id/:userId", isAllowedToChangeEvent, updateEvent);
router.delete("/:id/:userId", isAllowedToChangeEvent, removeEvent);

export { router };
