import mongoose from "mongoose";

const eventModel = new mongoose.Schema({
  description: {
    what: {
      type: String,
      required: true,
    },
    where: {
      type: String,
      required: true,
    },
    when: {
      type: Date,
      required: true,
    },
    cost: {
      type: Number,
      default: 0,
    },
  },
  isPublic: {
    type: Boolean,
    default: false,
  },
  isAllowedToSee: [
    {
      type: String,
      required: false,
    },
  ],
  user: {
    type: String,
    required: true,
  },
});

const model = mongoose.model("events", eventModel);

export { model };
