import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import { router as dataRouter } from "./data/data.router.js";
import { router as authRouter } from "./auth/auth.router.js";
import { router as userRouter } from "./user/user.router.js";
import { init as initAuth } from "./auth/initAuthentication.js";
import { isAuthenticated } from "./auth/authorization.js";

const app = express();

mongoose.connect(`${process.env.CONNECTION_STRING}`);

app.use(express.json());
app.use(bodyParser.json());

app.use(express.static("static"));

initAuth(app);

app.use("/auth/", authRouter);
app.use("/api/user", isAuthenticated, userRouter);
app.use("/api/data", isAuthenticated, dataRouter);

mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB");
  const port = process.env.PORT;
  app.listen(port, () => {
    console.log(`Server listens to http://localhost:${port}`);
  });
});
