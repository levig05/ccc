/*let events = [{ id: 1 }, { id: 2 }, { id: 3 }];

function getEveryEvent() {
  return events;
}

function getEventWithId(id) {
  return events.filter((event) => event.id === id);
}
*/

import mongoose from "mongoose";

const userModel = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  pwd: {
    type: String,
    required: true,
  },
});

const model = mongoose.model("users", userModel);

export { model };
