import { Router } from "express";
import { getUserIdByName, getUsernameById } from "./user.controller.js";

const router = Router();

router.get("/username/:username", getUserIdByName);
router.get("/id/:id", getUsernameById);

export { router };
