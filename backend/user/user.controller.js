import { model as User } from "./user.model.js";
import {
  findUserByNameAndCheckThatItExists,
  handleError,
  removeIdFromArrayOfIds,
} from "../controllerUtils.js";

function getUserIdByName(req, res) {
  const username = req.params.username;
  //console.log(username);
  User.findOne({ username: username })
    .then((user) => {
      //console.log(events);
      res.json({ _id: user._id });
    })
    .catch((error) => {
      handleError(res, error);
    });
}

function getUsernameById(req, res) {
  //console.log("helloworld", req);
  const id = req.params.id;
  //console.log(id);
  User.findOne({ _id: id })
    .then((user) => {
      res.json({ username: user.username });
    })
    .catch((error) => {
      handleError(res, error);
    });
}

export { getUserIdByName, getUsernameById };
